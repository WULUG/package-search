#!/bin/bash

confirm_fzf() {
	if [ ! -x /usr/bin/fzf ]
	then
		echo fuzzy finder is not available
		echo download with sudo apt-get install fzf
	fi
}

confirm_fzf

if [ $# -gt 0 ]
then
	if [ $1 == 'help' ]
	then
		echo search package will look for a package and allow you to narrow down results
		echo search by itself will let you look through the entire repo
	else
		apt-cache search $1 | fzf -i
	fi
else
	apt-cache search . | fzf -i
fi
