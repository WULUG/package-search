#!/bin/bash

# package search utility using fzf

confirm_fzf() {
    if [ ! -x /usr/bin/fzf ]
    then
        echo fzf is required to run this script
        echo you can download using pacman -Sy fzf 
    fi
}

confirm_fzf

if [ $# -gt 0 ]
then
    if [ $1 == 'aur' ]
    then
        yay -Sl | fzf -i --color=16
    elif [ $1 == 'help' ]
    then
        echo search aur will list aur repo
        echo search by itself will list the offical repos
        echo search package will search for a specific package
    else
        yay -Ss $1 | fzf -i --color=16
    fi
else
    pacman -Sl | fzf -i --color=16
fi
